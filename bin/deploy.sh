#!/bin/bash
# Deploy
git reset --hard HEAD~1
git pull
npm install
pm2 delete new-app
pm2 start ./server/index.js --name new-app
pm2 logs new-app