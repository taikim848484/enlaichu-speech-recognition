
const sqlite3 = require('sqlite3').verbose()
const db = new sqlite3.Database('localData.sql')

module.exports = {

  createTable: function () {
    console.log('> Initialize SQLiteDB')
    // db.run("CREATE TABLE IF NOT EXISTS conversation (uuid TEXT NOT NULL, type TEXT, result TEXT )");
    db.run('CREATE TABLE IF NOT EXISTS conversation ( ' +
      'uuid TEXT NOT NULL, ' +
      'test BOOLEAN, ' +
      'type TEXT, ' +
      'downloadPath TEXT, ' +
      'dbPath TEXT, ' +
      'dbTestPath TEXT, ' +
      'inputCodec TEXT, ' +
      'inputRate TEXT, ' +
      'ibmModel TEXT, ' +
      'counter INTEGER, ' +
      'inProgress BOOLEAN, ' +
      'textFieldName TEXT, ' +
      'startTime INTEGER, ' +
      'endTime INTEGER, ' +
      'eventParams TEXT, ' +
      'fullFifoFilePath TEXT, ' +
      'UNIQUE(uuid, counter))')
  },

  insertConversationSQL: function (uuid, test, type, path, dbPath, dbTestPath, inputCodec, inputRate, ibmModel, counter, inProgress, textFieldName, startTime, eventParams, fullFifoFilePath, callback) {
    db.run(
      'INSERT INTO conversation VALUES ( ' +
      '$uuid, ' +
      '$test, ' +
      '$type, ' +
      '$path, ' +
      '$dbPath, ' +
      '$dbTestPath, ' +
      '$inputCodec, ' +
      '$inputRate, ' +
      '$ibmModel, ' +
      '$counter, ' +
      '$inProgress, ' +
      '$textFieldName, ' +
      '$startTime, ' +
      'null, ' +
      '$eventParams, ' +
      '$fullFifoFilePath )',
      {
        $uuid: uuid,
        $test: test,
        $type: type,
        $path: path,
        $dbPath: dbPath,
        $dbTestPath: dbTestPath,
        $inputCodec: inputCodec,
        $inputRate: inputRate,
        $ibmModel: ibmModel,
        $counter: counter,
        $inProgress: inProgress,
        $textFieldName: textFieldName,
        $startTime: startTime,
        $eventParams: eventParams,
        $fullFifoFilePath: fullFifoFilePath
      },
      callback
    )
  },

  updateConversationProgressSQL: function (uuid, counter, inProgress, callback) {
    db.run('UPDATE conversation SET inProgress=$inProgress WHERE uuid=$uuid AND counter=$counter', { $uuid: uuid, $inProgress: inProgress, $counter: counter }, callback)
  },

  updateConvertedFilePathSQL: function (uuid, counter, path, callback) {
    console.log('update path')
    db.run('UPDATE conversation SET downloadPath=$path WHERE uuid=$uuid AND counter=$counter', { $uuid: uuid, $path: path, $counter: counter }, callback)
  },

  updateEndTime: function (uuid, counter, endTime, callback) {
    db.run('UPDATE conversation SET endTime=$endTime WHERE uuid=$uuid AND counter=$counter', { $uuid: uuid, $counter: counter, $endTime: endTime }, callback)
  },
  queryAll: function (queryString, callback) {
    db.all(queryString, callback)
  },
  queryFirst: function (queryString, callback) {
    db.get(queryString, callback)
  },

  isInProgress: function (uuid, callback) {
    db.get('SELECT uuid from conversation WHERE uuid=$uuid AND inProgress=$inProgress', { $uuid: uuid, $inProgress: true }, callback)
  },

  getInterruptedConversation: function (callback) {
    db.all('SELECT * from conversation WHERE inProgress=$inProgress', { $inProgress: true }, callback)
  }
}
