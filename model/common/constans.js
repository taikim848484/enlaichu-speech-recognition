const SOCKET_IO_EVENT = {
  CONNECTION: 'connection'
}

module.exports = {
  SOCKET_IO_EVENT
}
