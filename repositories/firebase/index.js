const firebase = require('firebase-admin')
const _ = require('lodash')
const { SpeechCommands, PhrasesFormatter } = require('./lib')

const FirebaseDB = function () {
  this.serviceAccount = require('../../speech-to-text-credentials-Phoneic-8bd0a3c20525.json')
  this.config = require('../../config')
  this.INTERMEDIATE_NOTIFICATION_INTERVAL = 2
  this.TEXT_FINAL_SENTENCE = 'Message sent. Please wait.'

  this.currentTranscriptsIBM = []
  this.currentTranscriptsGoogle = []
  this.currentAppendLocationCompletedTextIBM = []
  this.currentAppendLocationCompletedTextGoogle = []
  return this
}
FirebaseDB.prototype.init = function () {
  const self = this
  firebase.initializeApp({
    credential: firebase.credential.cert(self.serviceAccount),
    databaseURL: self.config.FIREBASE_DB_URL
  })
  console.log('> Initialize Firebase DB')
  self.rootRef = firebase.database().ref()
}

FirebaseDB.prototype.savePhoneCall = function (test, uuid, locationArr, inputCodec, inputRate) {
  const self = this
  const newLocationArr = test ? [...locationArr, `/speech-to-text-tests/${uuid}`] : locationArr
  newLocationArr.forEach(location => {
    self.rootRef.child(location).update({ inputCodec, inputRate })
  })
}

FirebaseDB.prototype.updatePhoneCallTranscripts = function (
  test,
  uuid,
  firebaseWriteSettings,
  requestType,
  sourceType,
  result,
  isError
) {
  const self = this
  const time = firebase.database.ServerValue.TIMESTAMP // new Date().getTime();
  const firebaseLocations = _.isArray(firebaseWriteSettings.firebaseLocations)
    ? _.compact(firebaseWriteSettings.firebaseLocations)
    : _.compact([firebaseWriteSettings.firebaseLocations])// remove holes and falsy (null, undefined, 0, -0, NaN, "", false, document.all)

  const firebaseQueueLocations = _.isArray(firebaseWriteSettings.firebaseQueueLocations)
    ? _.compact(firebaseWriteSettings.firebaseQueueLocations)
    : _.compact([firebaseWriteSettings.firebaseQueueLocations])// remove holes and falsy (null, undefined, 0, -0, NaN, "", false, document.all)

  const queuePassedInVariables = firebaseWriteSettings
    .queuePassedInVariables
  const firebaseIntermediateIntervalsQueueLocations = firebaseWriteSettings
    .firebaseIntermediateIntervalsQueueLocations
  const firebaseAppendLocation = _.isArray(firebaseWriteSettings.firebaseAppendLocation)
    ? _.compact(firebaseWriteSettings.firebaseAppendLocation)
    : _.compact([firebaseWriteSettings.firebaseAppendLocation])// remove holes and falsy (null, undefined, 0, -0, NaN, "", false, document.all)
  const firebaseAppendLocationParameter = firebaseWriteSettings
    .firebaseAppendLocationParameter
  const firebaseAppendLocationTextPrefix = firebaseWriteSettings.firebaseAppendLocationTextPrefix === undefined
    ? ''
    : firebaseWriteSettings.firebaseAppendLocationTextPrefix
  const listenForCommands = firebaseWriteSettings.listenForCommands
  const confirmMessageSent = firebaseWriteSettings.confirmMessageSent
  const responseQueueLocations = firebaseWriteSettings.responseQueueLocations

  // EC: Log each event to logging server
  console.log('Speech update location received')
  console.log(
    'original_firebase_locations: ' + firebaseWriteSettings.firebaseLocations
  )
  console.log('firebase_locations: ' + firebaseLocations)
  console.log(
    'original_firebase_queue_locations: ' +
    firebaseWriteSettings.firebaseQueueLocations
  )
  console.log('firebase_queue_locations:' + firebaseQueueLocations)
  console.log('firebase_append_location: ' + firebaseAppendLocation)
  console.log(
    'firebase_append_location_parameter:' +
    firebaseWriteSettings.firebaseAppendLocationParameter
  )
  console.log(
    'firebase_append_location_text_prefix:' +
    firebaseWriteSettings.firebaseAppendLocationTextPrefix
  )
  console.log('listenForCommands:' + firebaseWriteSettings.listenForCommands)
  console.log(
    'listenForCommandsResponse:' +
    firebaseWriteSettings.listenForCommandsResponse
  )
  console.log(
    'confirmMessageSent:' + firebaseWriteSettings.confirmMessageSent
  )
  console.log(
    'responseQueueLocations:' + firebaseWriteSettings.responseQueueLocations
  )

  let newLocationArr
  let currentTranscripts
  let currentAppendLocationCompletedText

  if (sourceType === 'ibm') {
    currentTranscripts = self.currentTranscriptsIBM
    currentAppendLocationCompletedText = self.currentAppendLocationCompletedTextIBM
    if (requestType !== sourceType) {
      newLocationArr = [`/speech-to-text-tests/${uuid}/ibmTranscripts`]
    } else {
      if (test) {
        newLocationArr = [
          ...firebaseLocations,
          `/speech-to-text-tests/${uuid}/ibmTranscripts`
        ]
      } else {
        newLocationArr = firebaseLocations
      }
    }
  } else if (sourceType === 'google') {
    currentTranscripts = self.currentTranscriptsGoogle
    currentAppendLocationCompletedText = self.currentAppendLocationCompletedTextGoogle
    if (requestType !== sourceType) {
      newLocationArr = [`/speech-to-text-tests/${uuid}/googleTranscripts`]
    } else {
      if (test) {
        newLocationArr = [
          ...firebaseLocations,
          `/speech-to-text-tests/${uuid}/googleTranscripts`
        ]
      } else {
        newLocationArr = firebaseLocations
      }
    }
  }

  let transcript = null

  // Get transcript from Firebase payload
  if (result) {
    if (result.final) {
      transcript = result.text
    } else {
      transcript = transcript || result.alternatives[0].text
    }
  }

  let conversObject
  const index = _.findIndex(
    currentTranscripts,
    transcript => transcript.uuid === uuid
  )
  let completedTextIndex = _.findIndex(
    currentAppendLocationCompletedText,
    completedTextObj => completedTextObj.uuid === uuid
  )

  if (transcript) {
    transcript = transcript.trim()
    const isFinal = result.final
    console.log(`>>> isFinal: ${isFinal}`)

    // let completedText = "";
    var completedText = ''
    if (completedTextIndex < 0) {
      currentAppendLocationCompletedText.push({ uuid, completedText })
      completedTextIndex = 0
    } else {
      completedText =
        currentAppendLocationCompletedText[completedTextIndex].completedText
    }

    let phraseId
    let startTime
    let completedTextFirebasePath
    if (isFinal) {
      // EC: update location
      if (firebaseAppendLocation && firebaseAppendLocationParameter) {
        console.log(`######### completedText: ${completedText}`)
        let formattedTranscript = PhrasesFormatter.format([
          {
            text: transcript
          }
        ])
        let allText = ''
        let writeText = ''
        // Now generate the full phrase
        /*
            let allText = completedText +
          ((completedText && formattedTranscript) ? ' ' : '') + //add a space if completedText and formattedTranscript both exist
          formattedTranscript;
            currentAppendLocationCompletedText[completedTextIndex].completedText = allText; //save the full phrase to be prepended to when next phrase comes in
            let writeText = firebaseAppendLocationTextPrefix + allText;
        */
        firebaseAppendLocation.forEach(eachFirebaseAppendLocation => {
          // Get the previous value from Firebase and then append the latest value and write back to the location
          completedTextFirebasePath =
            eachFirebaseAppendLocation +
            '/' +
            firebaseAppendLocationParameter
          self.rootRef
            .child(completedTextFirebasePath)
            .once('value', function (data) {
              completedText = data.val()
              console.log(
                `>>>>>>>>> completedTextFromFirebase (${completedTextFirebasePath}): ${completedText}`
              )
              allText = (completedText + ' ' + formattedTranscript).trim()
              writeText = (firebaseAppendLocationTextPrefix + allText).trim()
              self.rootRef
                .child(eachFirebaseAppendLocation)
                .update({ [firebaseAppendLocationParameter]: writeText })
              console.log(
                `######### wrote to (${eachFirebaseAppendLocation}): ${writeText}`
              )

              // First check if the location exists. Encountered previously that the recognition ended right before this write happened and the array didn't exist any more
              if (
                currentAppendLocationCompletedText[completedTextIndex]
                  .completedText !== undefined
              ) {
                // save the full phrase to be prepended to when next phrase comes in
                currentAppendLocationCompletedText[completedTextIndex].completedText = allText
              }
            })
        })
        /*
              //EC: Log each event to logging server

              ////var resp = client.post("", {
              "action": "Final text",
              "completedTextIndex": currentAppendLocationCompletedText,
              "completedTextGoogle": currentAppendLocationCompletedTextGoogle,
              "completedTextIBM": currentAppendLocationCompletedTextIBM,
              "firebaseAppendLocation": firebaseAppendLocation,
              "firebaseAppendLocationParameter": firebaseAppendLocationParameter,
              "transcript": formattedTranscript,
        "completedTextIndex": completedTextIndex,
        "completedText": completedText,
        "transcript": transcript,
        "formattedTranscript": formattedTranscript,
        "allText": allText,
              "writtenText": writeText,
              "data": conversObject
            }, {
              headers: {
                "Content-Type": "application/json"
              }
            });
        */
      }

      if (index < 0) {
        // Create first time
        // as well as last time

        phraseId = newLocationArr[0]
          ? self.rootRef.child(newLocationArr[0]).push().key
          : ''
        conversObject = self._generateConverseObject(
          'success',
          time,
          result,
          true,
          firebaseWriteSettings,
          phraseId
        )
      } else {
        phraseId = currentTranscripts[index].phraseId
        conversObject = self._generateConverseObject(
          'success',
          time,
          result,
          false,
          firebaseWriteSettings
        )

        currentTranscripts.splice(index, 1)
      }
    } else {
      // EC: intermediate text, append '...' to the end if last character is not puctuation character
      // let phrase = result.text; //PhrasesFormatter.format([{text:result.text}]); // This is old API to get result text if not final
      let phrase = result.alternatives[0].text // By our algorithm, the first alternatives will be the highest stability
      let lastChar = phrase.slice(-1)
      console.log('Phrase: ' + phrase + '\nLast char: ' + lastChar)
      if (lastChar && lastChar.match(/[^?.,]/)) {
        console.log("Does not match punctuation, so add '...'")
        result.alternatives[0].text += '...'
      }
      console.log('Intermediate phrase: ' + result.alternatives[0].text)
      if (index > -1) {
        phraseId = currentTranscripts[index].phraseId
        conversObject = self._generateConverseObject(
          'transcribing',
          time,
          result,
          false,
          firebaseWriteSettings
        )
      } else {
        startTime = Math.round(new Date().getTime() / 1000) // EC start time of next phrase
        phraseId = newLocationArr[0]
          ? self.rootRef.child(newLocationArr[0]).push().key
          : ''
        conversObject = self._generateConverseObject(
          'transcribing',
          time,
          result,
          true,
          firebaseWriteSettings,
          phraseId
        )
        currentTranscripts.push({ uuid, phraseId })
      }
    }

    newLocationArr = newLocationArr.map(
      location => `${location}/${phraseId}`
    )

    // Reserve key for final text, store it to transcribedKey
    // We will append later this path (contain the transcribedKey) to each locations,
    // so we only need to get key from the first one
    const transcribedKey = newLocationArr[0]
      ? self.rootRef.child(`${newLocationArr[0]}/transcribed_text_options`).push()
        .key
      : ''

    // EC: Log each event to logging server
    let unixtime = Math.round(new Date().getTime() / 1000)

    if (newLocationArr) {
      newLocationArr.forEach(location => {
        self.rootRef.child(location).update(conversObject)

        /*
            //EC: Log each event to logging server
            ////var resp = client.post("", {
              "STT_continuous_update": location,
              //"new_sentence_time" : newSentenceTime,
              "phraseId": phraseId,
              "index": index,
              "currentTranscripts": currentTranscripts,
              "currentTranscriptsGoogle": currentTranscriptsGoogle,
              "transcript": transcript,
              "startTime": startTime,
              "unixtime": unixtime,
              "duration": (unixtime - startTime),
              "data": conversObject
            }, {
              headers: {
                "Content-Type": "application/json"
              }
            });
        */

        if (isFinal && transcribedKey) {
          self.rootRef
            .child(`${location}/transcribed_text_options/${transcribedKey}`)
            .update(result)
        }
      })
    }

    // EC: Send intermediate duration events when duration reached
    if (
      !isFinal &&
      firebaseIntermediateIntervalsQueueLocations &&
      unixtime - startTime >= self.INTERMEDIATE_NOTIFICATION_INTERVAL
    ) {
      var intermediateQueueConversObject = self._generateConverseObject(
        'transcribing',
        time,
        result,
        true,
        firebaseWriteSettings,
        phraseId
      )
      var intermediateQueueObj = _.extend(queuePassedInVariables, {
        message: intermediateQueueConversObject,
        // command: "stt_intermediate_sentence",
        uuid: uuid
      })

      /* No need for the options for intermediate push notification
         intermediateQueueObj.message.transcribed_text_options = {
   [transcribedKey]: result
   }
*/
      if (firebaseIntermediateIntervalsQueueLocations) {
        firebaseIntermediateIntervalsQueueLocations.forEach(queueLocation => {
          /*
              //EC: Log each event to logging server
              ////var resp = client.post("", {
                "STT_intermediate_push": queueLocation,
                //"new_sentence_time" : newSentenceTime,
                //"location" : location,
                "phraseId": phraseId,
                "index": index,
                "startTime": startTime,
                "unixtime": unixtime,
                "duration": (unixtime - startTime),
                "data": intermediateQueueObj
              }, {
                headers: {
                  "Content-Type": "application/json"
                }
              });
          */

          self.rootRef.child(`${queueLocation}/`).push(intermediateQueueObj)
        })
      }

      startTime = unixtime // reset for previous time
    }

    if (isFinal && requestType === sourceType) {
      let textToSpeech = ''
      let soundFile = ''
      let actionCommand = ''

      // Check the spoken phrase for a command
      if (listenForCommands === 'true') {
        let finalAction = SpeechCommands.checkForCommand(transcript)

        if (finalAction.action) {
          console.log(
            `Detected action: + ${finalAction.action} || ${
              finalAction.textToSpeech
            } || ${finalAction.soundFile}`
          )
          // soundFile = finalAction.soundFile
          textToSpeech = finalAction.textToSpeech
          actionCommand = finalAction.action
          console.log(
            `Responding with action detected for ${transcript}: ${actionCommand} | TextToSpeech: ${textToSpeech}`
          )

          /*
      //Write the action to the conversation so it's visible
      let reminderPhraseId = newLocationArr[0] ? self.rootRef.child(newLocationArr[0]).push().key : "";
      let reminderConversObject = self._generateConverseObject('success', time, result, true, firebaseWriteSettings, reminderPhraseId);
      let queueObj = _.extend(queuePassedInVariables, {
    message: reminderConversObject,
    command: "stt_final_sentence",
    soundFile: soundFile,
    textToSpeech: textToSpeech,
    transcript: transcript,
    uuid: uuid
      });
      self.rootRef.child(`${queueLocation}/`).push(intermediateQueueObj);
      */
        } else if (confirmMessageSent === 'true') {
          // soundFile = SOUNDFILE_FINAL_SENTENCE;
          textToSpeech = self.TEXT_FINAL_SENTENCE
        }
      } else if (confirmMessageSent === 'true') {
        // soundFile = SOUNDFILE_FINAL_SENTENCE;
        textToSpeech = self.TEXT_FINAL_SENTENCE
      }

      const queueConversObject = self._generateConverseObject(
        'success',
        time,
        result,
        true,
        firebaseWriteSettings,
        phraseId
      )
      const queueObj = _.extend(queuePassedInVariables, {
        message: queueConversObject,
        command: 'stt_final_sentence',
        soundFile: soundFile,
        textToSpeech: textToSpeech,
        transcript: transcript,
        uuid: uuid
      })
      queueObj.message.transcribed_text_options = {
        [transcribedKey]: result
      }

      if ((textToSpeech || soundFile) && responseQueueLocations) {
        responseQueueLocations.forEach(queueLocation => {
          /*
              //EC: Log each event to logging server
              let unixtime = Math.round((new Date()).getTime() / 1000);
              ////var resp = client.post("", {
                "STT_response": queueLocation,
                //"new_sentence_time" : newSentenceTime,
                "phraseId": phraseId,
                "index": index,
                "unixtime": unixtime,
                "currentTranscripts": currentTranscripts,
                "transcript": transcript,
                "soundFile": soundFile,
                "textToSpeech": textToSpeech,
                "uuid": uuid,
                "data": queueObj
              }, {
                headers: {
                  "Content-Type": "application/json"
                }
              });
          */

          self.rootRef.child(`${queueLocation}/`).push(queueObj)
        })
      }

      if (firebaseQueueLocations) {
        firebaseQueueLocations.forEach(queueLocation => {
          self.rootRef.child(`${queueLocation}/`).push(queueObj)

          /*
              //EC: Log each event to logging server
              let unixtime = Math.round((new Date()).getTime() / 1000);
              ////var resp = client.post("", {
                "STT_final_update": queueLocation,
                //"new_sentence_time" : newSentenceTime,
                "phraseId": phraseId,
                "index": index,
                "unixtime": unixtime,
                "currentTranscripts": currentTranscripts,
                "transcript": transcript,
                "soundFile": soundFile,
                "textToSpeech": textToSpeech,
                "uuid": uuid,
                "data": queueObj
              }, {
                headers: {
                  "Content-Type": "application/json"
                }
              });
          */
        })
      }
    }
  } else if (isError) {
    // Update: Don't store error event payload onto Firebase
    //   let phraseId;
    //   if (index < 0) {
    //      Create first time
    //      as well as last time
    //     phraseId = self.rootRef.child(newLocationArr[0]).push().key;
    //     conversObject = self._generateConverseObject('error', time, null, true, firebaseWriteSettings, phraseId);
    //   } else {
    //     phraseId = currentTranscripts[index].phraseId;
    //     conversObject = self._generateConverseObject('error', time, null, false, firebaseWriteSettings);
    //     currentTranscripts.splice(index, 1);
    //   }
    //
    //   newLocationArr = newLocationArr.map((location) => `${location}/${phraseId}`);
    //
    //   if (newLocationArr) {
    //     newLocationArr.forEach((location) => {
    //       self.rootRef.child(location).update(conversObject);
    //     });
    //   }
    //
    //   if (requestType === sourceType) {
    //     const queueConversObject = self._generateConverseObject('error', time, result, true, firebaseWriteSettings, phraseId);
    //     const queueObj = _.extend(queuePassedInVariables, {
    //     message: queueConversObject,
    //     command: "stt_final_sentence",
    //     soundFile: "",
    //     textToSpeech: "",
    //     transcript: "",
    //     uuid: uuid
    //     });
    //     if (firebaseQueueLocations) {
    //       firebaseQueueLocations.forEach((queueLocation) => {
    //         self.rootRef.child(`${queueLocation}/`).push(queueObj);
    //       })
    //     }
    //   }
  }
}

FirebaseDB.prototype.endPhoneCall = function (uuid) {
  const self = this
  const completedTextIndexIBM = _.findIndex(
    self.currentAppendLocationCompletedTextIBM,
    completedTextObjIBM => completedTextObjIBM.uuid === uuid
  )
  if (completedTextIndexIBM > -1) {
    self.currentAppendLocationCompletedTextIBM.splice(completedTextIndexIBM, 1)
  }
  const completedTextIndexGoogle = _.findIndex(
    self.currentAppendLocationCompletedTextGoogle,
    completedTextObjGoogle => completedTextObjGoogle.uuid === uuid
  )
  if (completedTextIndexGoogle > -1) {
    self.currentAppendLocationCompletedTextGoogle.splice(
      completedTextIndexGoogle,
      1
    )
  }

  /*
  //EC: Log each event to logging server
var resp = client.post("", {
"action": "SpeechRecognitionStopped_RemoveCurrentAppendLocationCompletedText",
  "uuid": uuid,
  "completedTextIBM": currentAppendLocationCompletedTextIBM,
  "completedTextIndexIBM": completedTextIndexIBM,
  "completedTextGoogle": currentAppendLocationCompletedTextGoogle,
  "completedTextIndexGoogle": completedTextIndexGoogle
}, {
  headers: {
    "Content-Type": "application/json"
  }
});
  */
}

FirebaseDB.prototype.getLatestAddedItem = function (uuid) {
  const self = this
  let key
  self.rootRef.child(`/phone/transcriptions_test/${uuid}`)
    .orderByChild('date')
    .limitToLast(1)
    .on('child_added', function (snapshot) {
      console.log('>>>> HERE <<<<')
      console.log(snapshot.key)
      console.log(snapshot.val())
      key = snapshot.key
    })
  return key
}
FirebaseDB.prototype._generateConverseObject = function (
  trascriptionStatus,
  time,
  result,
  full,
  firebaseWriteSettings,
  phraseId
) {
  let conversObject
  const customFieldNames = firebaseWriteSettings.customNames
  const passedInVariables = firebaseWriteSettings.passedInVariables
  conversObject = {
    [customFieldNames.firebaseTimestampName]: time,
    transcription_status: trascriptionStatus
  }
  if (result) {
    conversObject[customFieldNames.fieldName] = result.final
      ? PhrasesFormatter.format([
        {
          text: result.text
        }
      ])
      : result.alternatives[0].text
  }

  if (full) {
    conversObject = _.extend(conversObject, {
      [customFieldNames.firebaseMessageIdName]: phraseId
    })
    conversObject = _.extend(conversObject, passedInVariables)
  }
  return conversObject
}

module.exports = new FirebaseDB()
