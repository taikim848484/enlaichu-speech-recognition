const PhrasesFormatter = {
  format: function (phrases) {
    if (phrases.length === 0) {
      return ''
    }

    return phrases
      .filter(function (phrase) {
        return phrase.text.trim() !== ''
      })
      .map(function (phrase) {
        var text = phrase.text.trim()
        text = this.capitalize(text)
        text = this.addSentenceSymbol(text)
        return text
      }, this)
      .join('\r\n')
  },
  formatOptions: function (phrasesOptions) {
    if (phrasesOptions.length === 0) {
      return []
    }

    var startStamp = phrasesOptions[0].startTime

    return phrasesOptions
      .filter(function (phraseOptions) {
        return phraseOptions.text.trim() !== ''
      })
      .map(function (phraseOptions) {
        var text = phraseOptions.text.trim()
        text = this.capitalize(text)
        text = this.addSentenceSymbol(text)

        var timeMarks = this._getTimeMarks(phraseOptions, startStamp)

        var alternatives = phraseOptions.alternatives
          .map(function (alternative) {
            var text = alternative.text.trim()
            text = this.capitalize(text)
            text = this.addSentenceSymbol(text)

            return {
              text: text,
              probability: alternative.probability
            }
          }.bind(this))

        return {
          startTime: timeMarks[0],
          endTime: timeMarks[1],
          text: text,
          alternatives: alternatives
        }
      }, this)
  },
  capitalize: function (phrase) {
    return phrase.replace(/\S/, function (m) {
      return m.toUpperCase()
    })
  },
  addSentenceSymbol: function (sentence) {
    var lastSymbol = sentence[sentence.length - 1]
    if (lastSymbol === '.' || lastSymbol === '!' || lastSymbol === '?') {
      return sentence
    }
    if (this._isQuestionSentence(sentence)) {
      return sentence + '?'
    } else {
      return sentence + '.'
    }
  },
  _isQuestionSentence: function (sentence) {
    var affixes = ['Could',
      'Couldn’t',
      'Shall',
      'Should',
      'Shouldn’t',
      'Would',
      'Wouldn’t',
      'Will',
      'Won’t',
      'Do',
      'Did',
      'Didn’t',
      'Does',
      'Doesn’t',
      'Am',
      'Is',
      'Isn’t',
      'Are',
      'Aren’t',
      'Can',
      'Can’t',
      'Was',
      'Wasn’t',
      'Were',
      'Weren’t',
      'Has',
      'Hasn’t',
      'Have',
      'Haven’t',
      'Had',
      'Hadn’t',
      'Who',
      'What',
      'When',
      'Where',
      'How',
      'Why',
      'Which',
      'Might']

    var blankAffixes = ['Or', 'And']

    var words = sentence.split(/\s+/)
    var word0 = words.length > 0 ? words[0].trim().toUpperCase() : ''
    var word1 = words.length > 1 ? words[1].trim().toUpperCase() : ''

    for (var i = 0; i < affixes.length; i++) {
      var affix = affixes[i].toUpperCase()
      if (word0 === affix) {
        return true
      }
      if (word1 === affix) {
        for (var j = 0; j < blankAffixes.length; j++) {
          var blankAffix = blankAffixes[j].toUpperCase()
          if (word0 === blankAffix) {
            return true
          }
        }
      }
    }

    return false
  },
  _getTimeMarks: function (phrase, startStamp) {
    var startTime = this._formatTime(phrase.startTime - startStamp)
    var endTime = phrase.endTime ? this._formatTime(phrase.endTime - startStamp) : 'xx:xx:xx'
    return [startTime, endTime]
  },
  _formatTime: function (time) {
    var seconds = Math.floor(time / 1000) % 60
    var minutes = Math.floor(time / (1000 * 60)) % 60
    var hours = Math.floor(time / (1000 * 60 * 60)) % 24

    hours = (hours < 10) ? '0' + hours : hours
    minutes = (minutes < 10) ? '0' + minutes : minutes
    seconds = (seconds < 10) ? '0' + seconds : seconds

    return hours + ':' + minutes + ':' + seconds
  }
}

const SpeechCommands = {
  checkForCommand: function (transcript) {
    console.log(`Checking ${transcript} for command`)

    const PHRASE_TRIGGER_WORD = 'PHRASE_TRIGGER_WORD'
    const PHRASE_CREATE_ACTION_ITEM = 'PHRASE_CREATE_ACTION_ITEM'
    const PHRASE_ADD_REMINDER = 'PHRASE_ADD_REMINDER'
    const PHRASE_START_RECORDING = 'PHRASE_START_RECORDING'
    const PHRASE_STOP_RECORDING = 'PHRASE_STOP_RECORDING'
    const PHRASE_ADD_NOTE = 'PHRASE_ADD_NOTE'
    const PHRASE_START_NOTE = 'PHRASE_START_NOTE'
    const PHRASE_STOP_NOTE = 'PHRASE_STOP_NOTE'

    const triggerWordPreStopWords = ['yo', 'hey', 'hi', 'hello', 'mr', 'mister', 'ms', 'miss', 'mrs']

    const triggerWordRegex = {
      matchPhrases: ['productive',
        'assistant',
        'assistance',
        'samantha',
        'cassandra'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/yes-question.wav',
      action: PHRASE_TRIGGER_WORD,
      noExtraWordsText: 'How can I help?',
      extraWordsPreText: '',
      extraWordsPostText: '',
      interactionNeeded: true,
      icon: ''
    }

    const actionPreStopWords = ['please can you', 'please', 'can you please', 'can you']
    const actionPostStopWords = ['please', 'thanks', 'thank you']

    const actionRegexArr = [{
      matchPhrases: ['create action item for',
        'create an action item for',
        'create an action item',
        'create action item',
        'take action item for',
        'take action item',
        'take an action item for',
        'take an action item',
        'take action item',
        'make an action item for',
        'make an action item',
        'add an action item to',
        'add an action item',
        'add new action item',
        'make an action item for',
        'make an action item',
        'make action item for',
        'make action item',
        'new action item',
        'action item for',
        'action item to',
        'action item',
        'create a new to do item',
        'create new to do item',
        'new to do item',
        'new to do'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/action-item-created.wav',
      action: PHRASE_CREATE_ACTION_ITEM,
      noExtraWordsText: 'Action item created',
      extraWordsPreText: 'Action item',
      extraWordsPostText: 'created',
      interactionNeeded: false,
      icon: '☐'
    },
    {
      matchPhrases: ['create a reminder for',
        'create reminder for',
        'create a reminder',
        'create reminder',
        'make a reminder for',
        'make reminder for',
        'remind me to',
        'remind me',
        'reminder for',
        'reminder to',
        'reminder',
        'new reminder',
        'add a reminder to',
        'add a reminder',
        'add reminder',
        'remind'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/reminder-created.wav',
      action: PHRASE_ADD_REMINDER,
      noExtraWordsText: 'Reminder created',
      extraWordsPreText: 'Reminder',
      extraWordsPostText: 'created',
      interactionNeeded: false,
      icon: ''
    },
    {
      matchPhrases: ['start recording',
        'recording start',
        'start call recording',
        'call recording on',
        'call recording',
        'record call',
        'recording on',
        'turn on recording',
        'turn recording on'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/call-recording-started.wav',
      action: PHRASE_START_RECORDING,
      noExtraWordsText: 'Call recording started',
      extraWordsPreText: '',
      extraWordsPostText: '',
      interactionNeeded: false,
      icon: ''
    },
    {
      matchPhrases: ['stopped recording',
        'stop recording',
        'recording stop',
        'stop call recording',
        'stop recording call',
        'stop recording call',
        'recording off',
        'terminate recording'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/call-recording-stopped.wav',
      action: PHRASE_STOP_RECORDING,
      noExtraWordsText: 'Call recording stopped',
      extraWordsPreText: '',
      extraWordsPostText: '',
      interactionNeeded: false,
      icon: ''
    },
    {
      matchPhrases: ['make a note to',
        'note to',
        'add a note to',
        'add a note',
        'add note to',
        'add note'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/note-added.wav',
      action: PHRASE_ADD_NOTE,
      noExtraWordsText: 'What would you like your note to say?',
      extraWordsPreText: 'Note',
      extraWordsPostText: 'created',
      interactionNeeded: true,
      icon: ''
    },
    {
      matchPhrases: ['start note',
        'start a note',
        'new note',
        'start a new note',
        'take a note',
        'take a new note',
        'take note',
        'take notes',
        'make a note',
        'create a note',
        'record a note'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/note-started.wav',
      action: PHRASE_START_NOTE,
      noExtraWordsText: 'Start dictating your note',
      extraWordsPreText: '',
      extraWordsPostText: '',
      interactionNeeded: false,
      icon: ''
    },
    {
      matchPhrases: ['stop note',
        'stop taking note',
        'stop taking a note',
        'stop taking the note',
        'stop new note',
        'stop note taking',
        'stop the note'
      ],
      soundFile: '/usr/local/freeswitch/sounds/custom/note-stopped.wav',
      action: PHRASE_STOP_NOTE,
      noExtraWordsText: 'Note created',
      extraWordsPreText: '',
      extraWordsPostText: '',
      interactionNeeded: false,
      icon: ''
    }

    ]

    let obj
    let text = transcript.trim()
    let regex
    let phrases
    let eachPhrase
    let matches
    let soundFile = false
    let action = false
    let extraWords = ''
    let textToSpeech = ''
    let extraWordsPreText = ''
    let extraWordsPostText = ''
    let noExtraWordsText = ''
    let interactionNeeded = false
    let icon = ''
    let triggerWord = ''

    // Remove punctuation
    text = text.replace(/[\.\?\,]/, '')

    // First detect if trigger word used
    phrases = triggerWordRegex.matchPhrases
    obj = triggerWordRegex
    for (j = 0; j < phrases.length; j++) {
      eachPhrase = phrases[j]
      regex = `^${eachPhrase}[\s\.\?]*(.*)?`
      // console.log(`Next phrase [${ j } of ${ phrases.length }]: "${ eachPhrase }"`);
      matches = text.match(new RegExp(regex, 'i'))
      // console.log(`Matching "${regex}" and "${text}" : ` + (matches ? matches[1] : ""));
      if (matches) {
        console.log(`MATCHED TRIGGER WORD: ${eachPhrase}: ${matches} / ${soundFile}`)
        action = obj.action
        extraWordsPostText = obj.extraWordsPostText
        extraWordsPreText = obj.extraWordsPreText
        noExtraWordsText = obj.noExtraWordsText
        interactionNeeded = obj.interactionNeeded
        triggerWord = eachPhrase
        icon = obj.icon
        soundFile = obj.soundFile
        break
      }
    }

    // Remove extraneous stop words before and after the command
    if (matches && matches[1]) {
      text = matches[1].trim()
      console.log(`Remaining text: ${text}`)
      actionPreStopWords.forEach(function (word) {
        regex = `^${word}`
        text = text.replace(new RegExp(regex, 'i'), '').trim()
        console.log(regex + ':::' + text)
      })
      actionPostStopWords.forEach(function (word) {
        regex = `${word}$`
        text = text.replace(new RegExp(regex, 'i'), '').trim()
        console.log(regex + ':::' + text)
      })
    }

    // Now see if we can match the core command
    matches = null
    for (i = 0; i < actionRegexArr.length; i++) {
      obj = actionRegexArr[i]
      phrases = obj.matchPhrases
      // console.log(`Next object [${i} of ${ actionRegexArr.length }]: ${ phrases}`);
      for (j = 0; j < phrases.length; j++) {
        eachPhrase = phrases[j]
        regex = `^${eachPhrase}[\s\.\?]*(.*)?`
        // console.log(`Next phrase [${ j } of ${ phrases.length }]: "${ regex }"`);
        matches = text.match(new RegExp(regex, 'i'))
        if (matches) {
          action = obj.action
          extraWordsPostText = obj.extraWordsPostText
          extraWordsPreText = obj.extraWordsPreText
          noExtraWordsText = obj.noExtraWordsText
          interactionNeeded = obj.interactionNeeded
          icon = obj.icon
          soundFile = obj.soundFile
          console.log(`${regex}: ${soundFile} || ${noExtraWordsText} || ${interactionNeeded} || ${icon}`)
          break
        }
      }
      if (matches) { break }
    }

    if (matches && matches[1]) { extraWords = matches[1].trim() }

    if (action) {
      if (!extraWords) {
        textToSpeech = noExtraWordsText
      } else {
        textToSpeech = `${extraWordsPreText} ${extraWords} ${extraWordsPostText}`
      }

      return {
        'soundFile': soundFile,
        'textToSpeech': textToSpeech,
        'action': action,
        'icon': icon,
        'interactionNeeded': interactionNeeded,
        'extraWords': extraWords
      }
    } else {
      return false
    }
  }
}

module.exports = {
  PhrasesFormatter,
  SpeechCommands
}
