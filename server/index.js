const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('../worker/socket.io')

const FreeSwitchConnection = require('../worker/freeswitch-connection').FreeSwitchConnection
const firebaseDatabase = require('../repositories/firebase')
const freeSwitchConnection = new FreeSwitchConnection()

const SQLiteDB = require('../model/SQLiteDB.js')

// socket io wrapper
io(server)

server.listen(4000, function () {
  console.log('Live at Port 4000')
})
// Initialize built-in database.
SQLiteDB.createTable()
// Initialize firebase database.
firebaseDatabase.init()
// Initialize freeswitch connection
freeSwitchConnection.start()
