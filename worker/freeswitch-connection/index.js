const ESL = require('modesl')
const fs = require('fs')
const ffmpeg = require('fluent-ffmpeg')

const rimraf = require('rimraf')
const _ = require('lodash')
const _FIFO = require('../../lib/fifo-js/fifo')
const Stream = require('stream')

const SQLiteDB = require('../../model/SQLiteDB.js')
// const FirebasePhonecall = require('../../repositories/firebase')
const firebaseDatabase = require('../../repositories/firebase')
const IBMSpeechToText = require('../ibm-engine')
const GoogleSpeechToText = require('../google-engine')
// const RemeetingSpeechToText = require("../remeeting-engine");

const currentPhoneStreams = []

const highBandwidthSettings = {
  ibmBandModel: 'BroadbandModel',
  fullFifoFileExtension: 'r16',
  ibmRate: '16000',
  googleRequestSampleRate: 16000,
  inputRate: '16000'
}

const lowBandwidthSettings = {
  ibmBandModel: 'NarrowbandModel',
  fullFifoFileExtension: 'r8',
  ibmRate: '8000',
  googleRequestSampleRate: 8000,
  googleRecognitionModel: 'phone_call',
  inputRate: '8000'
}

const FreeSwitchConnection = (exports.FreeSwitchConnection = function () {
  this.config = require('../../config')
  this.event = {
    ERROR: 'error',
    CUSTOM_EVENT: 'esl::event::CUSTOM::**',
    HANGUP_EVENT: 'esl::event::CHANNEL_HANGUP_COMPLETE::**',
    SPEECH_RECOGNITION_START: 'speech_recognition::start',
    SPEECH_RECOGNITION_STOP: 'speech_recognition::end'
  }
  this.eventType = {}
})

FreeSwitchConnection.prototype._init = function () {
  let self = this
  self.connectionInstance = new ESL.Connection(
    self.config.FS_IP_ADDRESS,
    self.config.FS_PORT,
    self.config.FS_PW,
    () => {
      console.log('Successfully connected to the freeswitch ...')
      console.log('--------------------------------------------')

      self._checkInteruptCall()
      self._eventsBinding()
    }
  ).on(self.event.ERROR, () => self._reconnect())
  self.readStreamIn = null
  self.writeStreamIn = null
  self.currentPhoneStreams = []
  self.streamCloneIBM = new Stream.PassThrough()
  self.streamCloneGoogle = new Stream.PassThrough()
  self.streamCloneFile = new Stream.PassThrough()
}

FreeSwitchConnection.prototype.start = function () {
  if (this.connectionInstance) return
  this._init()
}

FreeSwitchConnection.prototype._reconnect = function () {
  let self = this
  console.log('Error happened, trying to reconnect ...')
  setTimeout(function () {
    self._init()
  }, 5000)
}

FreeSwitchConnection.prototype._eventsBinding = function () {
  let self = this
  self.connectionInstance.events('json', 'all', () => self._eventsHandler())
}

FreeSwitchConnection.prototype._checkInteruptCall = function () {}

FreeSwitchConnection.prototype._eventsHandler = function () {
  let self = this
  self.connectionInstance
    .on(self.event.CUSTOM_EVENT, (value1, value2, eventJson) => {
      if (eventJson !== undefined) {
        const event = JSON.parse(eventJson)
        const d = new Date()
        // To output timestamps in console.log
        // catch event having subclass 'speech_recognition::start'
        console.log(d.toUTCString() + ' event name', event['Event-Name'])
        console.log('event subclass', event['Event-Subclass'])
        switch (event['Event-Subclass']) {
          case self.event.SPEECH_RECOGNITION_START: {
            console.log('==== event parameters ====')
            console.log(event)
            const uuid = event.variable_uuid
            const uuidOriginal = event.variable_uuid
            SQLiteDB.queryAll(
              `SELECT uuid, counter FROM conversation WHERE uuid="${uuid}"`,
              function (err, rows) {
                let currCounter
                let uuidExtend
                if (err || !rows || rows.length === 0) {
                  currCounter = 0
                  uuidExtend = uuidOriginal
                } else {
                  currCounter = rows[rows.length - 1].counter + 1
                  SQLiteDB.updateEndTime(uuid, currCounter - 1, Date.now())
                  uuidExtend = `${uuidOriginal}_${currCounter}`
                }

                // Variables
                let firebaseLocations =
                  event.variable_firebase_location instanceof Array
                    ? event.variable_firebase_location
                    : [event.variable_firebase_location]
                const language = event.variable_lang
                const engine = event.variable_engine
                const fieldName = event.variable_firebase_update_parameter
                const languageCode = event.variable_lang
                const interim_results = event.variable_interim_results === 'true'
                const engineBandwidth = event['variable_engine-bandwidth']
                const firebaseTimestampName = event.variable_firebase_timestamp_parameter
                const firebaseMessageIdName = event.variable_firebase_messageID_parameter
                const customFieldNames = [
                  fieldName,
                  firebaseTimestampName,
                  firebaseMessageIdName
                ]
                const parameterArr = event.variable_parameterArr
                const valueArr = event.variable_valueArr

                const test = event.variable_test === 'true'
                const saveLog = event.variable_log === 'true'
                if (!test && saveLog) {
                  firebaseLocations = [
                    ...firebaseLocations,
                    `/speech-to-text-tests/${uuid}`
                  ]
                }
                const saveFile = event.variable_save_file === 'true'

                const firebaseIntermediateIntervalsQueueLocations =
                  event.variable_firebase_intermediate_intervals_queue_location
                const firebaseQueueLocations =
                  event.variable_firebase_queue_location
                const queueParameterArr = event.variable_queueParameterArr
                const queueValueArr = event.variable_queueValueArr
                const firebaseAppendLocation =
                  event.variable_firebase_append_location
                const firebaseAppendLocationTextPrefix =
                  event.variable_firebase_append_location_text_prefix
                const firebaseAppendLocationParameter =
                  event.variable_firebase_append_location_parameter

                let passedInVariables = {}
                if (
                  parameterArr &&
                  valueArr &&
                  parameterArr.length > 0 &&
                  valueArr.length > 0
                ) {
                  parameterArr.forEach((parameterName, i) => {
                    passedInVariables = _.extend(passedInVariables, {
                      [parameterName]: valueArr[i]
                    })
                  })
                }

                let queuePassedInVariables = {}
                if (
                  queueParameterArr &&
                  queueValueArr &&
                  queueParameterArr.length > 0 &&
                  queueValueArr.length > 0
                ) {
                  queueParameterArr.forEach((queueParameterName, i) => {
                    queuePassedInVariables = _.extend(queuePassedInVariables, {
                      [queueParameterName]: queueValueArr[i]
                    })
                  })
                }

                const firebaseWriteSettings = {
                  firebaseLocations,
                  customNames: {
                    fieldName,
                    firebaseTimestampName,
                    firebaseMessageIdName
                  },
                  passedInVariables,
                  firebaseQueueLocations,
                  queuePassedInVariables,
                  firebaseIntermediateIntervalsQueueLocations,
                  firebaseAppendLocation,
                  firebaseAppendLocationParameter,
                  firebaseAppendLocationTextPrefix
                }

                // variable_firebase_queue_location: [ '/phone/test_queue/' ],
                // variable_queueParameterArr: [ 'testParameter', 'testuuid' ],
                // variable_queueValueArr:
                let fullFifoFilePath

                let ibmParams

                let googleRequestSampleRate
                let googleRequestCodec
                let googleRecognitionModel

                let inputFormat
                let inputRate

                // Info from phone call
                const codec = event['Channel-Read-Codec-Name']
                const rate = event['Channel-Read-Codec-Rate']
                const rateNumber = parseInt(rate)

                console.log(`>>> phone call codec: ${codec} `)
                console.log(`>>> phone call rate: ${rate} `)
                console.log(`>>> phone call rateNumber: ${rateNumber} `)

                // Path to temp file
                const fifoInFilePath = `/usr/local/freeswitch/recordings/${uuidExtend}`

                // Bandwidth settings
                let choosenBandwidthSettings
                if (rateNumber >= 16000) {
                  choosenBandwidthSettings = highBandwidthSettings
                } else {
                  choosenBandwidthSettings = lowBandwidthSettings
                }

                if (engineBandwidth) {
                  if (engineBandwidth === 'low') {
                    choosenBandwidthSettings = lowBandwidthSettings
                  } else if (engineBandwidth === 'high') {
                    choosenBandwidthSettings = highBandwidthSettings
                  }
                }

                fullFifoFilePath = `${fifoInFilePath}.${
                  choosenBandwidthSettings.fullFifoFileExtension
                }`

                ibmParams = {
                  content_type: `audio/l16;rate=${
                    choosenBandwidthSettings.ibmRate
                  }`,
                  model: `${languageCode}_${
                    choosenBandwidthSettings.ibmBandModel
                  }`,
                  inactivity_timeout: -1
                }

                googleRequestSampleRate =
                  choosenBandwidthSettings.googleRequestSampleRate
                googleRequestCodec = 'LINEAR16'
                googleRecognitionModel =
                  choosenBandwidthSettings.googleRecognitionModel

                inputFormat = 's16le'
                inputRate = choosenBandwidthSettings.inputRate

                // Download file path
                const fullDownloadFilePath = `${fifoInFilePath}.flac`

                // Create temp file
                // new FIFO(fullFifoFilePath);

                /* eslint-disable no-new */
                new _FIFO(fullFifoFilePath, '775')
                // take ownership that fifo file

                // Create first entry with basic data
                // FirebasePhonecall.savePhoneCall(test, uuid, firebaseLocations, codec, rate);
                // FirebasePhonecall.updateFirebaseMultiplePaths(test, uuidExtend, FirebasePhonecall.savePhoneCall, firebaseLocations, codec, rate);
                const clientTestTranscriptsPath = test
                  ? `/speech-to-text-tests/${uuid}`
                  : null
                SQLiteDB.insertConversationSQL(
                  uuid,
                  test,
                  engine,
                  saveFile ? fullDownloadFilePath : null,
                  firebaseLocations[0],
                  clientTestTranscriptsPath,
                  codec,
                  rate,
                  ibmParams.model,
                  currCounter,
                  true,
                  fieldName,
                  Date.now(),
                  JSON.stringify(event),
                  fullFifoFilePath
                )
                // emitToClient(null, 'new-conversation', {
                //   uuid,
                //   test,
                //   type: engine,
                //   dbPath: firebaseLocations[0],
                //   dbTestPath: clientTestTranscriptsPath,
                //   inputCodec: codec,
                //   inputRate: rate,
                //   inProgress: true,
                //   ibmModel: ibmParams.model,
                //   downloadPath: saveFile ? fullDownloadFilePath : null,
                //   counter: currCounter,
                //   textFieldName: fieldName,
                //   startTime: Date.now()
                // })

                // Call record_session cmd
                // Guide Freeswitch the location of the temp file
                console.log('​FreeSwitchConnection.prototype._eventsHandler -> fullFifoFilePath', fullFifoFilePath)
                self.connectionInstance.executeAsync(
                  'record_session',
                  fullFifoFilePath,
                  uuid,
                  function (evt) {
                    console.log('EXECUTE record_session')
                  }
                )

                // Create read/write stream
                self.readStreamIn = fs.createReadStream(fullFifoFilePath, {
                  encoding: null
                })
                self._readStreamEventsBinding(
                  engine,
                  test,
                  saveFile,
                  uuid,
                  fullFifoFilePath
                )
                self.currentPhoneStreams.push({
                  uuid,
                  path: fullFifoFilePath,
                  counter: currCounter
                })
                // Set up ibm and Google streams with params
                const recognizeStreamIBM = IBMSpeechToText.getIbmStreamingRecognize(
                  ibmParams,
                  firebaseDatabase.updatePhoneCallTranscripts,
                  firebaseDatabase.updatePhoneCallTranscripts,
                  interim_results,
                  test,
                  uuid,
                  firebaseWriteSettings,
                  engine,
                  'ibm'
                )

                const recognizeStreamGoogle = GoogleSpeechToText.getGoogleStreamingRecognize(
                  googleRequestCodec,
                  googleRequestSampleRate,
                  languageCode,
                  firebaseDatabase.updatePhoneCallTranscripts,
                  firebaseDatabase.updatePhoneCallTranscripts,
                  // FirebasePhonecall.updatePhoneCallError.bind(this, test, uuid, firebaseWriteSettings, engine, 'google') ,
                  self.streamCloneGoogle,
                  interim_results,
                  true,
                  googleRecognitionModel,
                  uuid,
                  firebaseWriteSettings,
                  test,
                  engine,
                  'google'
                )
                if (saveFile) {
                  self.writeStreamIn = fs.createWriteStream(
                    fullDownloadFilePath,
                    { encoding: null }
                  )
                  self._writeStreamEventsBinding()
                  const downloadFileStream = getConvertStream(
                    self.streamCloneFile,
                    inputFormat,
                    inputRate,
                    'flac',
                    'flac'
                  )
                  downloadFileStream.pipe(self.writeStreamIn)
                }
                // Begin stream to IBM and Google
                if (engine === 'google' || test) {
                  self.streamCloneGoogle.pipe(recognizeStreamGoogle)
                  // inputGoogleStream.pipe(recognizeStreamGoogle);
                  // inputIBMStream.pipe(recognizeStreamIBM);
                }
                if (engine === 'ibm' || test) {
                  self.streamCloneIBM.pipe(recognizeStreamIBM)
                  // inputIBMStream.pipe(recognizeStreamIBM);
                }
              }
            )
            break
          }
          case self.event.SPEECH_RECOGNITION_STOP: {
            console.log('> End event occured')
            console.log(`> UUID of End Event: ${event['Caller-Unique-ID']}`)
            const uuid = event.uuid || event['Caller-Unique-ID']
            let counter = 0

            const index = _.findIndex(
              currentPhoneStreams,
              stream => stream.uuid === uuid
            )

            if (index >= 0) {
              console.log('PATH', currentPhoneStreams[index].path)

              self.connectionInstance.executeAsync(
                'stop_record_session',
                currentPhoneStreams[index].path,
                uuid,
                function (evt) {
                  console.log('EXECUTE stop_record_session')
                }
              )
              counter = currentPhoneStreams[index].counter
              currentPhoneStreams.splice(index, 1)
            }

            firebaseDatabase.endPhoneCall(uuid)

            SQLiteDB.updateConversationProgressSQL(
              uuid,
              counter,
              false,
              // emitToClient.bind(this, null, 'session-finish', { uuid, counter })
              () => {}
            )
            break
          }
        }
      }
    })
    .on(self.event.HANGUP_EVENT, (value1, value2, eventJson) => {
      const event = JSON.parse(eventJson)
      const uuid = event.uuid || event['Caller-Unique-ID']
      let counter = 0

      const index = _.findIndex(
        currentPhoneStreams,
        stream => stream.uuid === uuid
      )

      if (index >= 0) {
        console.log('-=-=-=-== CALL HANGUP -=-=-=-==')
        self.connectionInstance.executeAsync(
          'stop_record_session',
          currentPhoneStreams[index].path,
          uuid,
          function (evt) {
            console.log('EXECUTE stop_record_session')
          }
        )
        counter = currentPhoneStreams[index].counter

        currentPhoneStreams.splice(index, 1)

        // EC Remove appended texts from FirebaseDb.js
        firebaseDatabase.endPhoneCall(uuid)
      }

      SQLiteDB.updateConversationProgressSQL(
        uuid,
        counter,
        false,
        () => {}
        // emitToClient.bind(this, null, 'session-finish', { uuid, counter })
      )
    })
}

FreeSwitchConnection.prototype._writeStreamEventsBinding = function (uuid, counter) {
  let self = this
  self.writeStreamIn
    .on('data', function (chunk) {
      // console.log('+ writeStreamIn DATA');
    })
    .on('end', function () {
      // console.log('+ writeStreamIn END');
    })
    .on('close', function () {
      // save downlaod file path
      // FirebasePhonecall.updateFirebaseMultiplePaths(test, uuidExtend, FirebasePhonecall.updatePhoneCallPath, firebaseLocations, fullDownloadFilePath);
      // emitToClient(null, 'download-finish', {
      //   uuid,
      //   counter
      // })
      // console.log('+ writeStreamIn CLOSE');
    })
    .on('error', function (err) {
      console.log('+ writeStreamIn error: ' + err)
    })
}

FreeSwitchConnection.prototype._readStreamEventsBinding = function (
  engine,
  test,
  saveFile,
  uuid,
  fullFifoFilePath) {
  let self = this
  console.log('​fullFifoFilePath', fullFifoFilePath)
  self.readStreamIn
    .on('data', function (chunk) {
      // console.log('+ readStreamIn DATA');
      // console.log(chunk);
      // dataCounter++
      // Copy data into clone streams
      try {
        if (engine === 'ibm' || test) {
          self.streamCloneIBM.write(chunk)
        }
        if (engine === 'google' || test) {
          self.streamCloneGoogle.write(chunk)
        }
        if (saveFile) {
          self.streamCloneFile.write(chunk)
        }
      } catch (e) {
        console.log('error', e)
      }
    })
    .on('end', function () {
      console.log('+ readStreamIn END')

      // Close all stream, delete files
      rimraf(fullFifoFilePath, function (err) {
        if (err) console.log(err)
        else console.log(`Done removing temp files of uuid: ${uuid}`)
      })
      // End clone stream
      self.streamCloneIBM.end()
      self.streamCloneGoogle.end()
      self.streamCloneFile.end()

      // console.log('>>> Data counter', dataCounter)
    })
    .on('close', function () {
      // console.log('+ readStreamIn CLOSE');

      // Close all stream, delete files
      rimraf(fullFifoFilePath, function (err) {
        if (err) console.log(err)
        else console.log(`Done removing temp files of uuid: ${uuid}`)
      })
      // End clone stream
      self.streamCloneIBM.end()
      self.streamCloneGoogle.end()
      self.streamCloneFile.end()

      console.log('!@#!@#!@# DATAA counter !@#@!#!@#')
      // console.log('data counter', dataCounter)
    })
    .on('error', function (err) {
      console.log('+ readstream error: ' + err)
    })
}

// ---------------------------------
//       FILE
// ----------------------------------
// ----------------------------------
// Pass data to client
// function emitToClient (clientId, eventName, data, error, options) {
//   const optionsCombinedData = Object.assign({}, data, options)
//   if (clientId === null) io.sockets.emit(eventName, optionsCombinedData)
//   else if (io.sockets.connected[clientId]) { io.sockets.connected[clientId].emit(eventName, optionsCombinedData) }
// }

// ---------------------------------
//       FFMPEG
// ----------------------------------
function getConvertStream (
  inputStream,
  inputFormat,
  inputRate,
  outputCodec,
  outputFormat,
  outputSampleRate
) {
  const inputOptions = inputRate
    ? ['-f ' + inputFormat, `-ar ${inputRate}`]
    : ['-f ' + inputFormat]
  return ffmpeg(inputStream)
    .inputOptions(inputOptions)
    .audioCodec(outputCodec)
    .format(outputFormat)
    .audioFrequency(outputSampleRate || inputRate)
    .on('error', err => console.error(err))
    .on('end', function () {
      console.log('+ ffmpeg convert end')
    })
}
