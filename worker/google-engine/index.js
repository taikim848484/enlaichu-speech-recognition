const _ = require('lodash')
// Imports the Google Cloud client library
const Speech = require('@google-cloud/speech')
const projectId = 'enlaichu-test'
const firebaseDatabase = require('../../repositories/firebase')

// Instantiates a client
let googleCloudClient = new Speech.SpeechClient({
  projectId: projectId,
  keyFilename: './Phoneic-prod-114eaf197919.json'
})

// const rootRef = require('../../repositories/firebase').rootRef

module.exports = {

  getGoogleStreamingRecognize: function (
    encoding,
    sampleRateHertz,
    languageCode,
    callbackData,
    callbackError,
    inputStream,
    interimResults,
    verbose,
    model,
    uuid,
    firebaseWriteSettings,
    test,
    requestType,
    sourceType
  ) {
    console.log('=== googleStreamingRecognize ==')
    console.log('encoding', encoding)
    console.log('sampleRateHertz', sampleRateHertz)

    // Listener to latest added item to FirebaseDb
    // TODO: Find better way to get latest immediate text
    let latestAddedItem = {}
    let restartCount = 0

    latestAddedItem.key = firebaseDatabase.getLatestAddedItem(uuid)
    const self = this

    const request = {
      config: {
        encoding: encoding,
        sampleRateHertz: sampleRateHertz,
        languageCode
      },
      interimResults,
      verbose,
      timeout: 300
    }

    if (model) {
      request.config.model = model
    }
    // EC: for logging
    /*
    var newSentenceTime = Math.round((new Date()).getTime() / 1000);
    ////var resp = client.post("", {"GoogleStreamingRecognize": "",
        "new_sentence_time" : newSentenceTime,
        //"unixtime": unixtime,
        //"data":conversObject
       },
         {headers: {"Content-Type":"application/json"}});
    */

    // Iterate over all locations from Firebase then update transcription_status to 'success'
    function saveLatestImmediateText () {
      const firebaseLocations = Array.isArray(firebaseWriteSettings.firebaseLocations)
        ? firebaseWriteSettings.firebaseLocations.filter(x => x)
        : [firebaseWriteSettings.firebaseLocations].filter(x => x) // remove holes and falsy (null, undefined, 0, -0, NaN, "", false, document.all)

      let newLocationArr

      if (sourceType === 'ibm') {
        if (requestType != sourceType) {
          newLocationArr = [`/speech-to-text-tests/${uuid}/ibmTranscripts`]
        } else {
          if (test) {
            newLocationArr = [
              ...firebaseLocations,
              `/speech-to-text-tests/${uuid}/ibmTranscripts`
            ]
          } else {
            newLocationArr = firebaseLocations
          }
        }
      } else if (sourceType === 'google') {
        if (requestType != sourceType) {
          newLocationArr = [`/speech-to-text-tests/${uuid}/googleTranscripts`]
        } else {
          if (test) {
            newLocationArr = [
              ...firebaseLocations,
              `/speech-to-text-tests/${uuid}/googleTranscripts`
            ]
          } else {
            newLocationArr = firebaseLocations
          }
        }
      }

      newLocationArr = newLocationArr.map((location) => `${location}/${latestAddedItem.key}`)

      // TODO:
      // 1. Not update field yet
      // 2. Don't get right key, value yet
      if (newLocationArr) {
        newLocationArr.forEach((location) => {
          console.log('>>> Gonna update latest added immediate text in case of over 65s stream')
          console.log(location)
          // rootRef.child(location).update({
          //   transcription_status: 'success',
          //   transcribed_text: latestAddedItem.text
          // }, (error) => {
          //   if (error) {
          //     console.log('> Error happen in update Firebase')
          //     console.error(error)
          //   } else {
          //     console.log('> Update transcription_status okay')
          //   }
          // })
        })
      };
    }

    function incrementRestartCount () {
      restartCount++
    }

    function createStream (request, options = { restartStream: false }) {
      const { restartStream } = options

      // return googleCloudClient.createRecognizeStream(request)
      return googleCloudClient.streamingRecognize(request)
        .on('error', (err) => {
          console.log(' ==== ERROR HAPPENS on .error createStream ===')
          console.log(err)
          if (err.code === 11 || err.code === 3) {
            // Update the current/ latest immediate text as final on Firebase
            // saveLatestImmediateText();

            if (inputStream) {
              console.log('>>> Prepare to create stream again <<<<')
              // Hard set verbose to receive full results
              request.verbose = verbose
              inputStream.pipe(createStream(request, { restartStream: true }))
            }
          } else if (callbackError) {
            firebaseDatabase.updatePhoneCallTranscripts(
              test,
              uuid,
              firebaseWriteSettings,
              requestType,
              sourceType,
              null,
              true,
              err
            )
          }
        })
        .on('data', (data) => {
          // console.log('> Google full data from createStream: ');
          // console.log(data);
          if (callbackData) {
            if (data.error) {
              console.log(' ==== ERROR HAPPENS on .data createStream ===')
              console.log(data.error)
              // Update: Don't restart stream pipe in this event, only restart on .error event
              // if (data.error.code === 11 || data.error.code === 3) {
              //   if (inputStream) {
              //     console.log('>>> Prepare to create stream again <<<<');
              //     // Hard set verbose to receive full results
              //     request.verbose = verbose;
              //     inputStream.pipe(createStream(request));
              //   }
              // } else
              if (callbackError) {
                firebaseDatabase.updatePhoneCallTranscripts(
                  test,
                  uuid,
                  firebaseWriteSettings,
                  requestType,
                  sourceType,
                  null,
                  true,
                  data.error.message
                )
              }
            } else if (data.results) {
              // saveLatestImmediateText();
              // console.log('> Google Data result: ');
              // console.log(JSON.stringify(data.results));
              if (data.results.length > 0) {
                const result = self.googleResultToFirebaseResult(data.results)

                // Save latest added intermediate text, if its not final text
                if (!result.final) {
                  if (!restartStream) {
                    latestAddedItem.text = result.alternatives[0].text
                  } else if (restartStream && restartCount === 0) {
                    // Add latest intermediate text to new stream with the same key from Firebase
                    result.alternatives[0].text = latestAddedItem.text + result.alternatives[0].text
                    incrementRestartCount()
                  }
                }

                firebaseDatabase.updatePhoneCallTranscripts(
                  test,
                  uuid,
                  firebaseWriteSettings,
                  requestType,
                  sourceType,
                  result,
                  false,
                  { hasDuration: true }
                )
              }
            }
          }
        })
    }
    return createStream(request)
  },
  googleResultToFirebaseResult: function (googleResult) {
    const firebaseResult = {
      alternatives: {}
    }

    firebaseResult.final = googleResult[0].isFinal

    if (firebaseResult.final) {
      firebaseResult.text = googleResult[0].alternatives[0].transcript
    } else {
      const descendingStabilityResults = googleResult.sort((prev, next) => prev.stability < next.stability)

      firebaseResult.alternatives = {
        '0': {
          text: descendingStabilityResults[0].alternatives[0].transcript,
          probability: descendingStabilityResults[0].alternatives[0].confidence
        }
      }

      // Remove first as the hightest stability transcript
      descendingStabilityResults.shift()

      descendingStabilityResults.map((res, i) => {
        let alternativeItem = {
          text: res.alternatives[0].transcript,
          probability: ''
        }
        if (res.alternatives[0].confidence) {
          alternativeItem.probability = res.alternatives[0].confidence
        }

        _.extend(firebaseResult.alternatives, {
          [(parseInt(i) + 1).toString()]: alternativeItem
        })
      })
    }

    console.log('============')
    console.log(firebaseResult)

    return firebaseResult
  }

}
