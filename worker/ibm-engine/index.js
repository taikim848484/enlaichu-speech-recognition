const _ = require('lodash')
const SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1')
const speech_to_text = new SpeechToTextV1({//eslint-disable-line
  username: 'cd857ed1-1fb0-4301-945b-4c2933c595c7',
  password: 'uKg6T2FMq4D5'
})

const firebaseDatabase = require('../../repositories/firebase')

const paramsIBM8 = {
  content_type: 'audio/flac;rate=8000',
  model: 'en-US_NarrowbandModel',
  inactivity_timeout: -1
}

const paramsIBM16 = {
  content_type: 'audio/flac;rate=16000',
  model: 'en-US_BroadbandModel',
  inactivity_timeout: -1
}

const paramsIBMMic = {
  content_type: 'audio/flac; rate=48000;',
  inactivity_timeout: -1
}

module.exports = {
  getIbmStreamingRecognize: function (
    parameter,
    callbackData,
    callbackError,
    interim,
    test,
    uuid,
    firebaseWriteSettings,
    requestType,
    sourceType
  ) {
    let tempTranscript
    const self = this

    const recognizeStream = speech_to_text.createRecognizeStream(parameter)
    recognizeStream.on('results', function (data) {
      console.log('+ IBM stream data full result: ', JSON.stringify(data, null, 4))
      if (data.results.length > 0) {
        const alternatives = data.results[0].alternatives
        const result = alternatives[0].transcript
        console.log('+ IBM stream data result: ', result)
        if (callbackData && result) {
          if (data.results[0].final || interim) {
            firebaseDatabase.updatePhoneCallTranscripts(
              test,
              uuid,
              firebaseWriteSettings,
              requestType,
              sourceType,
              self.ibmResultToFirebaseResult(data.results)
            )
          }
        }
      }
    })

    recognizeStream.on('error', function (part) {
      console.log('+ IBM error: ', part)

      if (callbackError) {
        if (typeof (part) === 'string') {
          firebaseDatabase.updatePhoneCallTranscripts(
            test,
            uuid,
            firebaseWriteSettings,
            requestType,
            sourceType,
            null,
            true,
            part
          )
        } else {
          if (part.raw && part.raw.data) {
            firebaseDatabase.updatePhoneCallTranscripts(
              test,
              uuid,
              firebaseWriteSettings,
              requestType,
              sourceType,
              null,
              true,
              part.raw.dat
            )
          } else {
            firebaseDatabase.updatePhoneCallTranscripts(
              test,
              uuid,
              firebaseWriteSettings,
              requestType,
              sourceType,
              null,
              true,
              callbackError(JSON.stringify(part))
            )
          }
        }
      }
    })

    recognizeStream.setEncoding('utf8'); // to get strings instead of Buffers from `data` events

    [
      // 'results',
      // 'data',
      'speaker_labels', 'close'].forEach(function (eventName) {
      recognizeStream.on(eventName, console.log.bind(console, '+ IBM ' + eventName + ' event: '))
    })
    return recognizeStream
  },

  ibmResultToFirebaseResult: function (ibmResult) {
    const firebaseResult = { alternatives: {} }
    const alternatives = ibmResult[0].alternatives
    firebaseResult.final = ibmResult[0].final
    firebaseResult.text = alternatives[0].transcript
    for (let i in alternatives) {
      let alternativeItem = {
        text: alternatives[i].transcript,
        probability: ''
      }
      if (alternatives[i].confidence) {
        alternativeItem.probability = alternatives[i].confidence
      }

      _.extend(firebaseResult.alternatives, {
        [i]: alternativeItem
      })
    }
    return firebaseResult
  }
}
